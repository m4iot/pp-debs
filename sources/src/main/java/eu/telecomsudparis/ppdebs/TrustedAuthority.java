/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): Denis Conan 
 */
package eu.telecomsudparis.ppdebs;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * This class defines the concept of Trusted Authority which generates and
 * distributes key to all the users of the system, including brokers.
 *
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 */
public class TrustedAuthority {
	/**
	 * initializes all the parameters of the system, including the public and
	 * private keys, the prime numbers and the random function that will be used in
	 * numerous classes.
	 *
	 * @param n         the number of bits for the Sophie Germain prime numbers.
	 * @param certainty the certainty of the prime quality of the generated Sophie
	 *                  Germain prime numbers.
	 * @return the parameters of the system, including the private and public keys
	 *         for searchable encryption and key policy, as well as the generated
	 *         Sophie Germain prime numbers and the random function.
	 * @param counter the number of times the test for generating Sophie Germain
	 *                prime numbers will be run.
	 * @throws NoSuchAlgorithmException.
	 */
	public Parameters initialization(final int n, final int certainty, final int counter)
			throws NoSuchAlgorithmException {
		Random randomFunction = new Random();
		BigInteger q = generateSophieGermainPrimes(n, counter, randomFunction, certainty);
		BigInteger p = q.multiply(BigInteger.valueOf(2)).add(BigInteger.valueOf(1));
		int r = randomFunction.nextInt();
		int seed = r == Integer.MIN_VALUE ? 0 : Math.abs(r);
		BigInteger secretSE;
		do {
			secretSE = new BigInteger(p.bitLength(), randomFunction);
		} while (secretSE.compareTo(p) >= 0);
		HashFunction h = new HashFunction("SHA-256");
		BigInteger generatorSE;
		do {
			generatorSE = new BigInteger(q.bitLength(), randomFunction);
		} while (generatorSE.compareTo(q) >= 0);
		generatorSE = BigInteger.valueOf(4).modPow(generatorSE, p);
		BigInteger orderSE = q;
		BigInteger randomElementSE = generatorSE.modPow(secretSE, p);
		BigInteger secretKP;
		do {
			secretKP = new BigInteger(q.bitLength(), randomFunction);
		} while (secretKP.compareTo(q) >= 0);
		BigInteger keyKP = generatorSE.modPow(secretKP, p);
		BigInteger randomElementKP;
		do {
			randomElementKP = new BigInteger(q.bitLength(), randomFunction);
		} while (randomElementKP.compareTo(q) >= 0);
		randomElementKP = BigInteger.valueOf(4).modPow(randomElementKP, p);
		BigInteger[] randomElementSetKP = new BigInteger[n];
		BigInteger start;
		for (int i = 0; i < n; i++) {
			do {
				start = new BigInteger(q.bitLength(), randomFunction);
			} while (start.compareTo(q) >= 0);
			randomElementSetKP[i] = BigInteger.valueOf(4).modPow(start, p);
		}
		PrimeNumbers primeNumbers = new PrimeNumbers(p, q);
		PublicKeySE publicKeySE = new PublicKeySE(generatorSE, orderSE, randomElementSE, h);
		PublicKeyKP publicKeyKP = new PublicKeyKP(keyKP, randomElementKP, randomElementSetKP);
		MasterKeySE masterKeySE = new MasterKeySE(secretSE, seed);
		MasterKeyKP masterKeyKP = new MasterKeyKP(secretKP);
		return new Parameters(publicKeySE, publicKeyKP, masterKeySE, masterKeyKP, primeNumbers, randomFunction);
	}

	/**
	 * generates the private and public key for a user, in order to encrypt
	 * attributes of attribute filters and re-encrypt them.
	 *
	 * @param userName   the name of the user.
	 * @param parameters the parameters defined during the initialization.
	 * @return the user key which contains the private and the public key of the
	 *         user.
	 */
	public UserKey keygen(final String userName, final Parameters parameters) {
		BigInteger usersPrivateKey;
		do {
			usersPrivateKey = new BigInteger(parameters.getMasterKeySE().getSecret().bitLength(),
					parameters.getRandomFunction());
		} while (usersPrivateKey.compareTo(parameters.getMasterKeySE().getSecret()) >= 0);

		BigInteger usersPublicKey = parameters.getMasterKeySE().getSecret().subtract(usersPrivateKey);
		PublicKey publicKey = new PublicKey(userName, usersPublicKey);
		PrivateKey privateKey = new PrivateKey(usersPrivateKey, parameters.getMasterKeySE().getSeed());
		return new UserKey(privateKey, publicKey);
	}

	/**
	 * generates a decryption filter, composed of decryption keys for a subscriber
	 * in order for the subscriber to be able to decrypt the received events if they
	 * match with his filter.
	 *
	 * @param identifierAttributeFilters the set of attribute filters of the
	 *                                   subscriber, because we only consider here
	 *                                   access trees with a root node associated
	 *                                   with the AND function.
	 * @param parameters                 the parameters defined during the
	 *                                   initialization.
	 * @return the decryption filter associated with the access tree of the
	 *         subscriber.
	 */
	public DecryptionKey[] createDecryptionKeys(final Attribute<?>[] identifierAttributeFilters,
			final Parameters parameters) {
		DecryptionKey[] decryptionKeys = new DecryptionKey[identifierAttributeFilters.length];
		BigInteger rx;
		BigInteger[] polynomial = new BigInteger[identifierAttributeFilters.length];
		polynomial[0] = parameters.getMasterKeyKP().getSecret();
		for (int i = 1; i < identifierAttributeFilters.length; i++) {
			polynomial[i] = new BigInteger(parameters.getPrimeNumbers().getP().bitLength(),
					parameters.getRandomFunction());
		}
		for (int i = 0; i < identifierAttributeFilters.length; i++) {
			do {
				rx = new BigInteger(parameters.getPrimeNumbers().getP().bitLength(), parameters.getRandomFunction());
			} while (rx.compareTo(parameters.getPrimeNumbers().getQ()) >= 0);
			decryptionKeys[i] = new DecryptionKey(
					parameters.getPublicKeyKP().getRandomElement().modPow(polynomialFunction(polynomial, i)
							.multiply(tFunction(identifierAttributeFilters[i], identifierAttributeFilters.length,
									parameters).modPow(rx, parameters.getPrimeNumbers().getQ()))
							.mod(parameters.getPrimeNumbers().getQ()), parameters.getPrimeNumbers().getP()),
					parameters.getPublicKeySE().getGenerator().modPow(rx, parameters.getPrimeNumbers().getP()));
		}
		return decryptionKeys;
	}

	/**
	 * defines a polynomial function.
	 *
	 * @param coeff the set of coefficients of the polynomial.
	 * @param a     the value the function will be applied on.
	 * @return the polynomial function applied on the input value.
	 */
	public BigInteger polynomialFunction(final BigInteger[] coeff, final int a) {
		BigInteger result = coeff[0];
		for (int i = 1; i < coeff.length; i++) {
			result = result.add(coeff[i].multiply(BigInteger.valueOf((long) Math.pow(a, i))));
		}
		return result;
	}

	/**
	 * defines the delta function that returns a BigInteger used in the T function.
	 *
	 * @param n                             the number of encrypted attributes.
	 * @param intSet                        the set of integers between 1 and n + 1.
	 * @param attributeFilterRepresentation the attribute representation as a
	 *                                      BigInteger.
	 * @return the result used later in the T function.
	 */
	public BigInteger deltaFunction(final int n, final int[] intSet, final BigInteger attributeFilterRepresentation) {
		BigInteger result = BigInteger.valueOf(1);
		for (int i : intSet) {
			if (i != n) {
				result = result.multiply(attributeFilterRepresentation.subtract(BigInteger.valueOf(n))
						.divide(BigInteger.valueOf((long) n - i)));
			}
		}
		return result;
	}

	/**
	 * defines the T function used to obtain the decryption filter.
	 *
	 * @param attributeFilter the attribute the T function will be applied on.
	 * @param n               the number of encrypted attributes.
	 * @param parameters      the parameters defined during the initialization.
	 * @return the T function applied on the attribute.
	 */
	public BigInteger tFunction(final Attribute<?> attributeFilter, final int n, final Parameters parameters) {
		BigInteger result = parameters.getPublicKeyKP().getRandomElement().modPow(
				new BigInteger((attributeFilter.getName() + attributeFilter.getValue().toString()).getBytes()).pow(n),
				parameters.getPrimeNumbers().getP());
		int[] intSet = new int[n + 1];
		for (int i = 0; i < n + 1; i++) {
			intSet[i] = i + 1;
		}
		for (int i : intSet) {
			result = result.multiply(parameters.getPublicKeyKP().getRandomElementSet()[i - 1].modPow(
					deltaFunction(i, intSet,
							new BigInteger(
									(attributeFilter.getName() + attributeFilter.getValue().toString()).getBytes())),
					parameters.getPrimeNumbers().getP()));
		}
		return result;
	}

	/**
	 * generates a Sophie Germain prime number of the required bit length and
	 * certainty.
	 *
	 * @param n              the bit length of the Sophie Germain prime number.
	 * @param randomFunction the random function generated during the initalization
	 *                       phase.
	 * @param certainty      the certainty associated with the prime quality of the
	 *                       generated Sophie Germain prime number. If we note n
	 *                       this certainty, the probability the generated number is
	 *                       not prime is (1 / 2 ^ n).
	 * @param k              the number of times the test will be run.
	 * @return the generated Sophie Germain prime number.
	 */
	public BigInteger generateSophieGermainPrimes(final int n, final int k, final Random randomFunction,
			final int certainty) {
		boolean bool = false;
		int counter = 0;
		BigInteger q = new BigInteger(n, randomFunction);
		BigInteger p;
		while (!bool && counter <= k) {
			counter = counter + 1;
			q = q.nextProbablePrime();
			p = q.multiply(BigInteger.valueOf(2)).add(BigInteger.valueOf(1));
			if (p.isProbablePrime(certainty) && q.isProbablePrime(certainty)) {
				bool = true;
			}
		}
		return q;
	}
}
