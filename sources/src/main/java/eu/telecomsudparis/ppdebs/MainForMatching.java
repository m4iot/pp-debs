/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): Denis Conan
 */
package eu.telecomsudparis.ppdebs;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * This is the Main class for matching process.
 * 
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 * @author Denis Conan
 *
 */
public final class MainForMatching {
	/**
	 * private empty constructor.
	 */
	private MainForMatching() {

	}

	/**
	 * the main method, used to run performance tests on the implementation,
	 * including a warm-up phase to collect garbage before the experiment.
	 * 
	 * @param args command line arguments.
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 * @throws InterruptedException
	 */
	public static void main(final String[] args)
			throws NoSuchAlgorithmException, UnsupportedEncodingException, InterruptedException {
		final int numberOfExperimentations = Integer.parseInt(System.getProperty("numberOfExperimentations"));
		final int numberOfExecutionsPerExperimentation = Integer
				.parseInt(System.getProperty("numberOfExecutionsPerExperimentation"));
		final int keyLength = Integer.parseInt(System.getProperty("keyLength"));
		final int numberOfAttributes = Integer.parseInt(System.getProperty("numberOfAttributes"));
		final int certainty = Integer.parseInt(System.getProperty("certainty"));
		final int counter = Integer.parseInt(System.getProperty("counter"));
		// warm up
		executeExperiment(10, numberOfExecutionsPerExperimentation, keyLength, numberOfAttributes, certainty, counter,
				false);
		// garbage collect before the experiment
		final long sleepTimeAfterGC = 5000;
		Thread.sleep(sleepTimeAfterGC);
		System.out.println("garbage collected, starting the experiment");
		System.out.println("number of experimentations = " + numberOfExperimentations);
		System.out.println("number of executions per experimentation = " + numberOfExecutionsPerExperimentation);
		System.out.println("");
		// experiment
		executeExperiment(numberOfExperimentations, numberOfExecutionsPerExperimentation, keyLength, numberOfAttributes,
				certainty, counter, true);
	}

	/**
	 * executes the experiment to measure performance for the matching on encrypted
	 * and non-encrypted attributes.
	 * 
	 * @param numberOfExperimentations             the number of experimentations.
	 * @param numberOfExecutionsPerExperimentation the number of executions per
	 *                                             experimentation.
	 * @param keyLength                            the key length used for
	 *                                             encryption.
	 * @param numberOfAttributes                   the number of attributes of the
	 *                                             PIA.
	 * @param certainty                            the certainty of the
	 *                                             initialization phase.
	 * @param counter                              the counter for the
	 *                                             initialization phase.
	 * @param measure
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 */
	public static void executeExperiment(final int numberOfExperimentations,
			final int numberOfExecutionsPerExperimentation, final int keyLength, final int numberOfAttributes,
			final int certainty, final int counter, final boolean measure)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		// initialization f the constants
		final String publisherName = "Nathanaël";
		final String subscriberName = "Pierre";
		final String prefixOfAttributeNames = "attr";
		TrustedAuthority trustedAuthority = new TrustedAuthority();
		Broker broker = new Broker(0);
		Publisher publisher = new Publisher(publisherName);
		Subscriber subscriber = new Subscriber(subscriberName);
		Attribute<?>[] attributes = new Attribute[numberOfAttributes];
		for (int i = 0; i < numberOfAttributes; i++) {
			attributes[i] = new Attribute<>(prefixOfAttributeNames + i, 1000, -1);
		}
		String[] identifiers = new String[numberOfAttributes / 3];
		String[] quasiIdentifiers = new String[numberOfAttributes / 3];
		String[] nonSensitives = new String[numberOfAttributes / 3];
		for (int i = 0; i < (numberOfAttributes / 3); i++) {
			identifiers[i] = attributes[i].getName();
			quasiIdentifiers[i] = attributes[i + (numberOfAttributes / 3)].getName();
			nonSensitives[i] = attributes[i + 2 * (numberOfAttributes / 3)].getName();
		}
		PIA pia = new PIA(identifiers, quasiIdentifiers, nonSensitives);
		Proxy proxy = new Proxy(pia);
		// initialization of arrays
		long[][] matchingForIdentifiersTimes = new long[numberOfAttributes / 3][numberOfExperimentations];
		long[][] matchingForQuasiIdentifiersTimes = new long[numberOfAttributes / 3][numberOfExperimentations];
		long[][] matchingForNonSensitivesTimes = new long[numberOfAttributes / 3][numberOfExperimentations];
		long matchingTime;
		// initialization by the Trusted Authority
		Parameters parameters = trustedAuthority.initialization(keyLength, certainty, counter);
		// user key generation by the Trusted Authority
		UserKey publisherKey = trustedAuthority.keygen(publisher.getName(), parameters);
		publisher.setKeys(publisherKey.getPrivateKey(), publisherKey.getPublicKey());
		UserKey subscriberKey = trustedAuthority.keygen(subscriber.getName(), parameters);
		subscriber.setKeys(subscriberKey.getPrivateKey(), subscriberKey.getPublicKey());
		for (int i = 1; i <= numberOfAttributes / 3; i++) {
			// initialization of the attributes of the publisher
			Attribute<?>[] publisherAttributes = new Attribute[i * 3];
			for (int j = 0; j < i; j++) {
				publisherAttributes[j] = attributes[j];
				publisherAttributes[i + j] = attributes[(numberOfAttributes / 3) + j];
				publisherAttributes[2 * i + j] = attributes[(numberOfAttributes / 3) * 2 + j];
			}
			// initialization of the attribute filters of the subscriber
			Attribute<?>[] subscriberAttributes = new Attribute[i * 3];
			for (int j = 0; j < i; j++) {
				subscriberAttributes[j] = attributes[j];
				subscriberAttributes[i + j] = attributes[(numberOfAttributes / 3) + j];
				subscriberAttributes[2 * i + j] = attributes[(numberOfAttributes / 3) * 2 + j];
			}
			// splitting of the attributes by the proxy on publisher side
			Attribute<?>[][] publisherSplittedAttributes = proxy.splitAttributes(publisherAttributes);
			// splitting of the attributes by the proxy on subscriber side
			Attribute<?>[][] subscriberSplittedAttributes = proxy.splitAttributes(subscriberAttributes);
			// encryption of filter by the subscriber
			EncryptedAttributeFilter[] encryptedAttributeFilters = subscriber
					.encryptIdentifierAttributeFilters(subscriberSplittedAttributes[0], parameters);
			// re-encryption of filter by the subscriber access broker
			ReEncryptedAttributeFilter[] reEncryptedAttributeFilters = broker
					.reEncryptIdentifierAttributeFilters(encryptedAttributeFilters, subscriber, parameters);
			// encryption of event by the publisher
			EncryptedEvent encryptedEvent = publisher.encryptIdentifierAttributes(publisherSplittedAttributes[0],
					parameters);
			// re-encryption of event by the publisher access broker
			ReEncryptedEvent reEncryptedEvent = broker.reEncryptIdentifierAttributes(encryptedEvent, publisher,
					parameters);
			for (int j = 0; j < numberOfExperimentations; j++) {
				ForwardedEncryptedEvent[] forwardedEncryptedEventsArray = new ForwardedEncryptedEvent[numberOfExecutionsPerExperimentation];
				Attribute<?>[][] forwardedNonSensitiveAttributesArray = new Attribute<?>[numberOfExecutionsPerExperimentation][numberOfAttributes
						/ 3];
				Attribute<?>[] forwardedQuasiIdentifierAttributesArray = new Attribute<?>[numberOfExecutionsPerExperimentation];
				matchingTime = System.nanoTime();
				for (int k = 0; k < numberOfExecutionsPerExperimentation; k++) {
					// matching on identifier attributes by the broker
					forwardedEncryptedEventsArray[k] = broker.encryptedMatching(reEncryptedEvent,
							reEncryptedAttributeFilters, parameters);
				}
				matchingTime = System.nanoTime() - matchingTime;
				// collect of the average time for an experimentation
				matchingForIdentifiersTimes[i - 1][j] = matchingTime / numberOfExecutionsPerExperimentation;
				matchingTime = System.nanoTime();
				for (int k = 0; k < numberOfExecutionsPerExperimentation; k++) {
					// matching on non-sensitive attributes by the broker
					forwardedNonSensitiveAttributesArray[k] = broker.matchingOnNonSensitiveAttributes(
							publisherSplittedAttributes[2], subscriberSplittedAttributes[2]);
				}
				matchingTime = System.nanoTime() - matchingTime;
				// collect of the average time for an experimentation
				matchingForNonSensitivesTimes[i - 1][j] = matchingTime / numberOfExecutionsPerExperimentation;
				matchingTime = System.nanoTime();
				for (int l = 0; l < numberOfExecutionsPerExperimentation; l++) {
					// matching on quasi-identifier attributes by the broker
					for (Attribute<?> attributeSubscriber : subscriberSplittedAttributes[1]) {
						for (Attribute<?> attributePublisher : publisherSplittedAttributes[1]) {
							forwardedQuasiIdentifierAttributesArray[l] = broker
									.matchingOnQuasiIdentifierAttributes(attributePublisher, attributeSubscriber);
						}
					}
				}
				matchingTime = System.nanoTime() - matchingTime;
				// collect of the average time for an experimentation
				matchingForQuasiIdentifiersTimes[i - 1][j] = matchingTime / numberOfExecutionsPerExperimentation;
			}
			if (measure) {
				// printing results : average time and standard deviation for matching on
				// identifier attributes, quasi-identifier attributes and non sensitive
				// attributes
				System.out.println("average matching on identifier attributes time for " + keyLength + " bits with " + i
						+ " attributes : " + calculateAverage(matchingForIdentifiersTimes[i - 1]));
				System.out.println("standard deviation on matching on identifier attributes time for " + keyLength
						+ " bits with " + i + " attributes : "
						+ calculateStandardDeviation(matchingForIdentifiersTimes[i - 1]));
				System.out
						.println("average matching on quasi-identifier attributes time for " + keyLength + " bits with "
								+ i + " attributes : " + calculateAverage(matchingForQuasiIdentifiersTimes[i - 1]));
				System.out.println("standard deviation on matching for quasi-identifier attributes time for "
						+ keyLength + " bits with " + i + " attributes : "
						+ calculateStandardDeviation(matchingForQuasiIdentifiersTimes[i - 1]));
				System.out.println("average matching on non-sensitive attributes time for " + keyLength + " bits with "
						+ i + " attributes : " + calculateAverage(matchingForNonSensitivesTimes[i - 1]));
				System.out.println("standard deviation on matching for non-sensitive attributes time for " + keyLength
						+ " bits with " + i + " attributes : "
						+ calculateStandardDeviation(matchingForNonSensitivesTimes[i - 1]));
				System.out.println("");
			}
		}
	}

	/**
	 * calculate the average time of an array of times.
	 * 
	 * @param times the array of times.
	 * @return the average time of the array of times.
	 */
	public static float calculateAverage(final long[] times) {
		float average = 0;
		for (long time : times) {
			average += time;
		}
		average = average / times.length;
		return average;
	}

	/**
	 * calculate the standard deviation for an array of times.
	 * 
	 * @param times the array of times.
	 * @return the standard deviation for the array of times.
	 */
	public static float calculateStandardDeviation(final long[] times) {
		float standardDeviation = 0;
		for (long time : times) {
			standardDeviation += Math.pow(time - calculateAverage(times), 2);
		}
		standardDeviation = standardDeviation / times.length;
		standardDeviation = (float) Math.sqrt(standardDeviation);
		return standardDeviation;
	}
}
