/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): 
 */
package eu.telecomsudparis.ppdebs;

import java.util.ArrayList;

/**
 * This class defines the different processes of the proxy, both on
 * publisher-side and subscriber-side.
 *
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 */
public class Proxy {
	/**
	 * the PIA associated with the proxy.
	 */
	private PIA pia;

	/**
	 * constructs a proxy.
	 *
	 * @param pia0 the PIA associated with the proxy.
	 */
	public Proxy(final PIA pia0) {
		this.pia = pia0;
	}

	/**
	 * gets the PIA associated with the proxy.
	 *
	 * @return pia the PIA associated with the proxy.
	 */
	public PIA getPIA() {
		return this.pia;
	}

	/**
	 * sets the PIA associated with the proxy.
	 *
	 * @param pia0 the PIA to set.
	 */
	public void setPIA(final PIA pia0) {
		this.pia = pia0;
	}

	/**
	 * splits the attributes (or attribute filters) according to the associated PIA.
	 *
	 * @param attributes the set of incoming attributes (or attribute filters).
	 * @return three sets of attributes, one for identifiers, one for
	 *         quasi-identifiers and one for non sensitives.
	 */
	public Attribute<?>[][] splitAttributes(final Attribute<?>[] attributes) {
		ArrayList<Attribute<?>> identifierAttributes0 = new ArrayList<>();
		ArrayList<Attribute<?>> quasiIdentifierAttributes0 = new ArrayList<>();
		ArrayList<Attribute<?>> nonSensitiveAttributes0 = new ArrayList<>();
		for (Attribute<?> attribute : attributes) {
			if (this.pia.checkIdentifier(attribute) >= 0) {
				attribute.setId(this.pia.checkIdentifier(attribute));
				identifierAttributes0.add(attribute);
			} else if (this.pia.checkQuasiIdentifier(attribute) >= 0) {
				attribute.setId(this.pia.checkQuasiIdentifier(attribute));
				quasiIdentifierAttributes0.add(attribute);
			} else if (this.pia.checkNonSensitive(attribute) >= 0) {
				attribute.setId(this.pia.checkNonSensitive(attribute));
				nonSensitiveAttributes0.add(attribute);
			}
		}
		Attribute<?>[] identifierAttributes = new Attribute<?>[identifierAttributes0.size()];
		identifierAttributes0.toArray(identifierAttributes);
		Attribute<?>[] quasiIdentifierAttributes = new Attribute<?>[quasiIdentifierAttributes0.size()];
		quasiIdentifierAttributes0.toArray(quasiIdentifierAttributes);
		Attribute<?>[] nonSensitiveAttributes = new Attribute<?>[nonSensitiveAttributes0.size()];
		nonSensitiveAttributes0.toArray(nonSensitiveAttributes);

		return new Attribute<?>[][]{ identifierAttributes, quasiIdentifierAttributes, nonSensitiveAttributes };
	}
}
