/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): 
 */
package eu.telecomsudparis.ppdebs;

import java.math.BigInteger;

/**
 * This class defines the concept of publisher in a very simple way, only taking
 * into account the user keys possessed by a publisher. Publishers publish
 * notifications encrypted with some specific attributes called identifiers.
 *
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 *
 */
public class Publisher {
	/**
	 * the name of the publisher.
	 */
	private String name;
	/**
	 * the private key of the publisher.
	 */
	private PrivateKey privateKey;
	/**
	 * the public key of the publisher.
	 */
	private PublicKey publicKey;

	/**
	 * constructs a publisher.
	 *
	 * @param name0 the name of the publisher.
	 */
	public Publisher(final String name0) {
		this.name = name0;
	}

	/**
	 * gets the name of the publisher.
	 *
	 * @return the name of the publisher.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * gets the private key of the publisher.
	 *
	 * @return the private key of the publisher.
	 */
	public PrivateKey getPrivateKey() {
		return this.privateKey;
	}

	/**
	 * gets the public key of the publisher.
	 *
	 * @return the public key of the publisher.
	 */
	public PublicKey getPublicKey() {
		return this.publicKey;
	}

	/**
	 * sets the user keys of the publisher.
	 *
	 * @param privateKey the private key of the publisher.
	 * @param publicKey  the public key of the publisher.
	 */
	public void setKeys(final PrivateKey privateKey, final PublicKey publicKey) {
		this.privateKey = privateKey;
		this.publicKey = publicKey;
	}

	/**
	 * encrypts a set of identifier attributes as well as the message contained in
	 * the notification.
	 *
	 * @param identifierAttributes the set of identifier attributes that are going
	 *                             to be encrypted.
	 * @param parameters           the parameters defined by the Trusted Authority.
	 * @return an encrypted event with the set of encrypted attributes, a key, the
	 *         encrypted message of the notification and a set of shared secrets
	 *         obtained by the T function.
	 */
	public EncryptedEvent encryptIdentifierAttributes(final Attribute<?>[] identifierAttributes,
			final Parameters parameters) {
		SharedSecret[] tFunctionOnAttributes = new SharedSecret[identifierAttributes.length];
		for (int i = 0; i < identifierAttributes.length; i++) {
			tFunctionOnAttributes[i] = tFunction(identifierAttributes[i], identifierAttributes.length, parameters);
		}
		BigInteger s;
		do {
			s = new BigInteger(parameters.getPrimeNumbers().getQ().bitLength(), parameters.getRandomFunction());
		} while (s.compareTo(parameters.getPrimeNumbers().getQ()) >= 0);
		return new EncryptedEvent(trapDoorsGeneration(identifierAttributes, parameters), BigInteger.valueOf(0),
				parameters.getPublicKeySE().getGenerator().modPow(s, parameters.getPrimeNumbers().getP()),
				tFunctionOnAttributes);
	}

	/**
	 * generates the trap doors for an encrypted vent.
	 * 
	 * @param identifierAttributes the identifier attributes which are going to be
	 *                             encrypted.
	 * @param parameters           the parameters defined by the Trusted Authority.
	 * @return the set of trap doors for the corresponding encrypted event.
	 */
	public TrapDoorForPublisher[] trapDoorsGeneration(final Attribute<?>[] identifierAttributes,
			final Parameters parameters) {
		TrapDoorForPublisher[] trapDoors = new TrapDoorForPublisher[identifierAttributes.length];
		BigInteger ra;
		for (int i = 0; i < identifierAttributes.length; i++) {
			do {
				ra = new BigInteger(parameters.getPrimeNumbers().getQ().bitLength(), parameters.getRandomFunction());
			} while (ra.compareTo(parameters.getPrimeNumbers().getQ()) >= 0);
			trapDoors[i] = new TrapDoorForPublisher(
					parameters
							.getPublicKeySE().getGenerator().modPow(
									pseudoRandomFunction(identifierAttributes)[i].subtract(ra), parameters
											.getPrimeNumbers().getP()),
					parameters.getPublicKeySE().getGenerator().modPow(
							this.getPublicKey().getKey().multiply(ra)
									.add(this.getPrivateKey().getKey()
											.multiply(pseudoRandomFunction(identifierAttributes)[i])),
							parameters.getPrimeNumbers().getP()),
					identifierAttributes[i].getId());
		}
		return trapDoors;
	}

	/**
	 * defines the delta function that returns a BigInteger used in the T function.
	 *
	 * @param n                       the number of encrypted attributes.
	 * @param intSet                  the set of integers between 1 and n + 1.
	 * @param attributeRepresentation the attribute representation as a BigInteger.
	 * @return the result used later in the T function.
	 */
	public BigInteger deltaFunction(final int n, final int[] intSet, final BigInteger attributeRepresentation) {
		BigInteger result = BigInteger.valueOf(1);
		for (int i : intSet) {
			if (i != n) {
				result = result.multiply(attributeRepresentation.subtract(BigInteger.valueOf(n))
						.divide(BigInteger.valueOf((long) n - i)));
			}
		}
		return result;
	}

	/**
	 * defines the T function used to obtain the set of shared secrets of the
	 * encrypted event.
	 *
	 * @param attribute  the attribute the T function will be applied on.
	 * @param n          the number of encrypted attributes.
	 * @param parameters the parameters defined by the Trusted Authority.
	 * @return the T function applied on the attribute.
	 */
	public SharedSecret tFunction(final Attribute<?> attribute, final int n, final Parameters parameters) {
		BigInteger result = parameters.getPublicKeyKP().getRandomElement().modPow(
				new BigInteger((attribute.getName() + attribute.getValue().toString()).getBytes()).pow(n),
				parameters.getPrimeNumbers().getP());
		int[] intSet = new int[n + 1];
		for (int i = 0; i < n + 1; i++) {
			intSet[i] = i + 1;
		}
		for (int i : intSet) {
			result = result.multiply(parameters.getPublicKeyKP().getRandomElementSet()[i - 1].modPow(
					deltaFunction(i, intSet,
							new BigInteger((attribute.getName() + attribute.getValue().toString()).getBytes())),
					parameters.getPrimeNumbers().getP()));
		}
		return new SharedSecret(result, attribute.getId());
	}

	/**
	 * defines the pseudo random function of the user. This is the same function
	 * than the one implemented on subscriber side.
	 *
	 * @param attributes the set of attributes the pseudo random function will be
	 *                   applied on.
	 * @return the set of attributes once the have been through the pseudo random
	 *         function.
	 */
	public BigInteger[] pseudoRandomFunction(final Attribute<?>[] attributes) {
		byte[] seedInBytes = BigInteger.valueOf(this.getPrivateKey().getSeed()).toByteArray();
		BigInteger[] attributesRepresentation = new BigInteger[attributes.length];
		for (int i = 0; i < attributes.length; i++) {
			attributesRepresentation[i] = pseudoRandomFunctionOnAttribute(seedInBytes,
					(attributes[i].getName() + attributes[i].getValue().toString()).getBytes());
		}
		return attributesRepresentation;
	}

	/**
	 * defines the pseudo random function of the user applied on a single attribute.
	 *
	 * @param seedInBytes      the seed of the pseudo random function, in bytes.
	 * @param attributeInBytes the attribute the pseudo random function will be
	 *                         applied on, in bytes.
	 * @return the pseudo random function applied on the attribute.
	 */
	public BigInteger pseudoRandomFunctionOnAttribute(final byte[] seedInBytes, final byte[] attributeInBytes) {
		String attributeInBinary = new BigInteger(attributeInBytes).toString(2);
		StringBuilder seedInBinary = new StringBuilder(new BigInteger(seedInBytes).toString(2));
		while (seedInBinary.length() < attributeInBinary.length()) {
			seedInBinary.append(seedInBinary);
		}
		StringBuilder result = new StringBuilder("");
		for (int i = 0; i < attributeInBinary.length(); i++) {
			if (attributeInBinary.charAt(i) == seedInBinary.charAt(i)) {
				result.append("1");
			} else {
				result.append("0");
			}
		}
		return new BigInteger(result.toString(), 2);
	}
}
