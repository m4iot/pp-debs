/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): 
 */
package eu.telecomsudparis.ppdebs;

import java.math.BigInteger;

/**
 * This class defines the concept of encrypted event, which is used by the
 * publishers to publish encrypted identifier attributes with the encrypted
 * message of the notification.
 *
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 *
 */
public class EncryptedEvent {
	/**
	 * the set of trapdoors of the encrypted event.
	 */
	private TrapDoorForPublisher[] trapDoors;
	/**
	 * the encrypted messag of the encrypted event.
	 */
	private BigInteger encryptedMessage;
	/**
	 * the key of the encrypted event, used during the decryption operation.
	 */
	private BigInteger key;
	/**
	 * the set of shared secrets of the encrypted event, obtained by the T function.
	 */
	private SharedSecret[] tFunctionOnAttributes;

	/**
	 * constructs an encrypted event.
	 *
	 * @param trapDoors0           the set of trapdoors of the encrypted event.
	 * @param encryptedMessage0    the encrypted message of the encrypted event.
	 * @param key0                 the key of the encrypted event.
	 * @param tFunctionAttributes0 the set of shared secrets of the encrypted event.
	 */
	public EncryptedEvent(final TrapDoorForPublisher[] trapDoors0, final BigInteger encryptedMessage0,
			final BigInteger key0, final SharedSecret[] tFunctionAttributes0) {
		this.trapDoors = trapDoors0;
		this.encryptedMessage = encryptedMessage0;
		this.key = key0;
		this.tFunctionOnAttributes = tFunctionAttributes0;
	}

	/**
	 * gets the trapdoors of the encrypted event.
	 *
	 * @return the set of trapdoors of the encrypted event.
	 */
	public TrapDoorForPublisher[] getTrapDoors() {
		return this.trapDoors;
	}

	/**
	 * gets the encrypted message of the encrypted event.
	 *
	 * @return the encrypted message of the encrypted event.
	 */
	public BigInteger getEncryptedMessage() {
		return this.encryptedMessage;
	}

	/**
	 * gets the key of the encrypted event.
	 *
	 * @return the key of the encrypted event.
	 */
	public BigInteger getKey() {
		return this.key;
	}

	/**
	 * gets the shared secrets of the encrypted event.
	 *
	 * @return the set of shared secrets of the encrypted event.
	 */
	public SharedSecret[] getTFunctionOnAttributes() {
		return this.tFunctionOnAttributes;
	}
}
