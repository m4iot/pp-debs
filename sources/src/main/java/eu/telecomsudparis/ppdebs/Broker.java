/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): 
 */
package eu.telecomsudparis.ppdebs;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class defines the methods used by the brokers to re-encrypt events and
 * filters, but also to perform matching on encrypted and clear data.
 *
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 */
public class Broker {
	/**
	 * the id of the broker.
	 */
	private int id;

	/**
	 * construct a broker.
	 * 
	 * @param id0 the id of the broker.
	 */
	public Broker(final int id0) {
		this.id = id0;
	}

	/**
	 * gets the id of the broker.
	 *
	 * @return the id of the broker.
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * re-encrypts an event already encrypted by a publisher.
	 *
	 * @param encryptedEvent the incoming encrypted event.
	 * @param publisher      the publisher who encrypted the event.
	 * @param parameters     the parameters defined by the Trusted Authority.
	 * @return a re-encrypted event.
	 */
	public ReEncryptedEvent reEncryptIdentifierAttributes(final EncryptedEvent encryptedEvent,
			final Publisher publisher, final Parameters parameters) {
		TrapDoorForBroker[] trapDoors = new TrapDoorForBroker[encryptedEvent.getTrapDoors().length];
		for (int i = 0; i < encryptedEvent.getTrapDoors().length; i++) {
			trapDoors[i] = new TrapDoorForBroker(encryptedEvent.getTrapDoors()[i].getTD1()
					.modPow(publisher.getPublicKey().getKey(), parameters.getPrimeNumbers().getP())
					.multiply(encryptedEvent.getTrapDoors()[i].getTD2()).mod(parameters.getPrimeNumbers().getP()),
					encryptedEvent.getTrapDoors()[i].getId());
		}
		return new ReEncryptedEvent(trapDoors, encryptedEvent.getEncryptedMessage(), encryptedEvent.getKey(),
				encryptedEvent.getTFunctionOnAttributes());
	}

	/**
	 * re-encrypts a set of attribute filters already encrypted by a subscriber.
	 * 
	 * @param encryptedAttributeFilters the incoming set of attribute filters
	 *                                  already encrypted.
	 * @param subscriber                the subscriber who encrypted the set of
	 *                                  attribute filters.
	 * @param parameters                the parameters defined by the Trusted
	 *                                  Authority.
	 * @return a set of re-encrypted attribute filters.
	 */
	public ReEncryptedAttributeFilter[] reEncryptIdentifierAttributeFilters(
			final EncryptedAttributeFilter[] encryptedAttributeFilters, final Subscriber subscriber,
			final Parameters parameters) {
		ReEncryptedAttributeFilter[] reEncryptedIdentifierAttributeFilters = new ReEncryptedAttributeFilter[encryptedAttributeFilters.length];
		for (int i = 0; i < encryptedAttributeFilters.length; i++) {
			reEncryptedIdentifierAttributeFilters[i] = new ReEncryptedAttributeFilter(
					encryptedAttributeFilters[i].getCB1()
							.modPow(subscriber.getPublicKey().getKey(), parameters.getPrimeNumbers().getP())
							.multiply(encryptedAttributeFilters[i].getCB2()).mod(parameters.getPrimeNumbers().getP()),
					encryptedAttributeFilters[i].getHash(), encryptedAttributeFilters[i].getId());
		}
		return reEncryptedIdentifierAttributeFilters;
	}

	/**
	 * performs the matching on encrypted data.
	 *
	 * @param reEncryptedEvent                      the incoming re-encrypted event.
	 * @param reEncryptedIdentifierAttributeFilters the incoming set of re-encrypted
	 *                                              attribute filters.
	 * @param parameters                            the parameters defined by the
	 *                                              Trusted Authority.
	 * @return a forwarded encrypted event to the subscriber if matching occurs.
	 */
	public ForwardedEncryptedEvent encryptedMatching(final ReEncryptedEvent reEncryptedEvent,
			final ReEncryptedAttributeFilter[] reEncryptedIdentifierAttributeFilters, final Parameters parameters) {
		ArrayList<SharedSecret> sharedSecrets0 = new ArrayList<>();
		byte[] bytechain;
		byte[] hash;
		for (int i = 0; i < reEncryptedIdentifierAttributeFilters.length; i++) {
			for (int j = 0; j < reEncryptedEvent.getTrapDoors().length; j++) {
				if (reEncryptedIdentifierAttributeFilters[i].getId() == reEncryptedEvent.getTrapDoors()[j].getId()) {
					bytechain = reEncryptedIdentifierAttributeFilters[i].getCB()
							.multiply(reEncryptedEvent.getTrapDoors()[j].getTD()
									.modInverse(parameters.getPrimeNumbers().getP()))
							.mod(parameters.getPrimeNumbers().getP()).toString().getBytes(StandardCharsets.UTF_8);
					parameters.getPublicKeySE().getHashFunction().getMessageDigest().update(bytechain);
					hash = parameters.getPublicKeySE().getHashFunction().getMessageDigest().digest();
					if (Arrays.equals(reEncryptedIdentifierAttributeFilters[i].getHash(), hash)) {
						sharedSecrets0.add(reEncryptedEvent.getTFunctionOnAttributes()[j]);
					}
				}
			}
		}
		if (sharedSecrets0.size() != reEncryptedIdentifierAttributeFilters.length) {
			return null;
		}
		SharedSecret[] sharedSecrets = new SharedSecret[sharedSecrets0.size()];
		sharedSecrets0.toArray(sharedSecrets);
		return new ForwardedEncryptedEvent(reEncryptedEvent.getEncryptedMessage(), reEncryptedEvent.getKey(),
				sharedSecrets);
	}

	/**
	 * performs matching on the non sensitive attributes set.
	 *
	 * @param nonSensitiveAttributes       the set of non sensitive attributes.
	 * @param nonSensitiveAttributeFilters the set of non sensitive attribute
	 *                                     filters.
	 * @return the non sensitive attributes composing the filter if matching occurs.
	 */
	public Attribute<?>[] matchingOnNonSensitiveAttributes(final Attribute<?>[] nonSensitiveAttributes,
			final Attribute<?>[] nonSensitiveAttributeFilters) {
		ArrayList<Attribute<?>> forwardedNonSensitiveAttributes0 = new ArrayList<>();
		for (int i = 0; i < nonSensitiveAttributeFilters.length; i++) {
			for (int j = 0; j < nonSensitiveAttributes.length; j++) {
				if (nonSensitiveAttributeFilters[i].getId() == nonSensitiveAttributes[j].getId()
						&& nonSensitiveAttributeFilters[i].getName().contentEquals(nonSensitiveAttributes[j].getName())
						&& nonSensitiveAttributeFilters[i].getValue().equals(nonSensitiveAttributes[j].getValue())) {
					forwardedNonSensitiveAttributes0.add(nonSensitiveAttributes[j]);
				}
			}
		}
		if (forwardedNonSensitiveAttributes0.size() != nonSensitiveAttributeFilters.length) {
			return null;
		}
		Attribute<?>[] forwardedNonSensitiveAttributes = new Attribute<?>[forwardedNonSensitiveAttributes0.size()];
		forwardedNonSensitiveAttributes0.toArray(forwardedNonSensitiveAttributes);
		return forwardedNonSensitiveAttributes;
	}

	/**
	 * performs matching on a quasi-identifier attribute.
	 *
	 * @param quasiIdentifierAttribute       the quasi-identifier attribute.
	 * @param quasiIdentifierAttributeFilter the quasi identifier attribute filter.
	 * @return the attribute if matching occurs.
	 */
	public Attribute<?> matchingOnQuasiIdentifierAttributes(final Attribute<?> quasiIdentifierAttribute,
			final Attribute<?> quasiIdentifierAttributeFilter) {
		if (quasiIdentifierAttributeFilter.getName().contentEquals(quasiIdentifierAttribute.getName())
				&& quasiIdentifierAttributeFilter.getValue().equals(quasiIdentifierAttribute.getValue())) {
			return quasiIdentifierAttribute;
		}
		return null;
	}
}
