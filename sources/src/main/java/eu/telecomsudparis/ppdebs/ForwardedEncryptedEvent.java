/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): 
 */
package eu.telecomsudparis.ppdebs;

import java.math.BigInteger;

/**
 * This class defines the concept of forwarded encrypted event, which is used by
 * the subscriber to finally decrypt the message of the notification.
 *
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 *
 */
public class ForwardedEncryptedEvent {
	/**
	 * the encrypted message of the forwarded encrypted event.
	 */
	private BigInteger encryptedMessage;
	/**
	 * the key of the forwarded encrypted event.
	 */
	private BigInteger key;
	/**
	 * the set of shared secrets of the forwarded encrypted event, obtained by the T
	 * function.
	 */
	private SharedSecret[] tFunctionOnAttributes;

	/**
	 * constructs a forwarded encrypted event.
	 *
	 * @param encryptedMessage0      the encrypted message of the forwarded
	 *                               encrypted event.
	 * @param key0                   the key of the forwarded encrypted event.
	 * @param tFunctionOnAttributes0 the set of shared secrets of the forwarded
	 *                               encrypted event.
	 */
	public ForwardedEncryptedEvent(final BigInteger encryptedMessage0, final BigInteger key0,
			final SharedSecret[] tFunctionOnAttributes0) {
		this.encryptedMessage = encryptedMessage0;
		this.key = key0;
		this.tFunctionOnAttributes = tFunctionOnAttributes0;
	}

	/**
	 * gets the encrypted message of the forwarded encrypted event.
	 *
	 * @return the encrypted message of the forwarded encrypted event.
	 */
	public BigInteger getEncryptedMessage() {
		return this.encryptedMessage;
	}

	/**
	 * gets the key of the forwarded encrypted event.
	 *
	 * @return the key of the forwarded encrypted event.
	 */
	public BigInteger getKey() {
		return this.key;
	}

	/**
	 * gets the shared secrets of the forwarded encrypted event.
	 *
	 * @return the set of shared secrets of the forwarded encrypted event.
	 */
	public SharedSecret[] getTFunctionOnAttributes() {
		return this.tFunctionOnAttributes;
	}
}
