/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): 
 */
package eu.telecomsudparis.ppdebs;

import java.math.BigInteger;

/**
 * This class defines the concept of master key for key policy which is
 * generated by the Trusted Authority during the initialization phase.
 *
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 */
public class MasterKeyKP {
	/**
	 * the secret of the master key for key policy.
	 */
	private BigInteger secret;

	/**
	 * constructs a master key for key policy.
	 *
	 * @param secret0 the secret of the master key for key policy.
	 */
	public MasterKeyKP(final BigInteger secret0) {
		this.secret = secret0;
	}

	/**
	 * gets the secret of the master key for key policy.
	 *
	 * @return the secret of the master key for key policy.
	 */
	public BigInteger getSecret() {
		return this.secret;
	}
}
