/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): 
 */
package eu.telecomsudparis.ppdebs;

import java.math.BigInteger;

/**
 * This class defines the concept of private key, which is the private key of a
 * user, generated by the Trusted Authority with a keygen function.
 *
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 *
 */
public class PrivateKey {
	/**
	 * the key of the private key.
	 */
	private BigInteger key;
	/**
	 * the seed of the private key, used in the pseudo random function.
	 */
	private int seed;

	/**
	 * constructs a private key.
	 *
	 * @param key0  the key of the private key.
	 * @param seed0 the seed of the private key.
	 */
	public PrivateKey(final BigInteger key0, final int seed0) {
		this.key = key0;
		this.seed = seed0;
	}

	/**
	 * gets the key of the private key.
	 *
	 * @return the key of the private key.
	 */
	public BigInteger getKey() {
		return this.key;
	}

	/**
	 * gets the seed of the private key.
	 *
	 * @return the seed of the private key.
	 */
	public int getSeed() {
		return this.seed;
	}
}
