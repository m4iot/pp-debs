/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): Denis Conan
 */
package eu.telecomsudparis.ppdebs;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * This is the Main class for the initialization phase.
 * 
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 * @author Denis Conan
 */
public final class MainForInitialization {
	/**
	 * private empty constructor.
	 */
	private MainForInitialization() {

	}

	/**
	 * the main method, used to run performance tests on the implementation,
	 * including a warm-up phase to collect garbage before the experiment.
	 * 
	 * @param args
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 * @throws InterruptedException
	 */
	public static void main(final String[] args) throws NoSuchAlgorithmException, InterruptedException {
		final int numberOfExperimentations = Integer.parseInt(System.getProperty("numberOfExperimentations"));
		final int numberOfExecutionsPerExperimentationForKeygen = Integer
				.parseInt(System.getProperty("numberOfExecutionsPerExperimentationForKeygen"));
		final int certainty = Integer.parseInt(System.getProperty("certainty"));
		final int counter = Integer.parseInt(System.getProperty("counter"));
		// warm up
		executeExperiment(10, numberOfExecutionsPerExperimentationForKeygen, certainty, counter, false);
		// garbage collect before the experiment
		System.gc();
		final long sleepTimeAfterGC = 5000;
		Thread.sleep(sleepTimeAfterGC);
		System.out.println("garbage collected, starting experiment");
		System.out.println("number of experimentations = " + numberOfExperimentations);
		System.out.println("number of executions per experimentation for key generation = "
				+ numberOfExecutionsPerExperimentationForKeygen);
		System.out.println("");
		// experiment
		executeExperiment(numberOfExperimentations, numberOfExecutionsPerExperimentationForKeygen, certainty, counter,
				true);
	}

	/**
	 * executes the experiment to measure performance for the initialization phase
	 * run by the Trusted Authority.
	 * 
	 * @param numberOfExperimentations                      the number of
	 *                                                      experimentations.
	 * @param numberOfExecutionsPerExperimentationForKeygen the number of executions
	 *                                                      per experimentation for
	 *                                                      key generation.
	 * @param certainty                                     the certainty of the
	 *                                                      initialization phase.
	 * @param counter                                       the counter for the
	 *                                                      initialization phase.
	 * @param measure
	 * @throws NoSuchAlgorithmException
	 */
	public static void executeExperiment(final int numberOfExperimentations,
			final int numberOfExecutionsPerExperimentationForKeygen, final int certainty, final int counter,
			final boolean measure) throws NoSuchAlgorithmException {
		// initialization of the constants
		final String publisherName = "Nathanaël";
		final String subscriberName = "Pierre";
		Publisher publisher = new Publisher(publisherName);
		Subscriber subscriber = new Subscriber(subscriberName);
		TrustedAuthority trustedAuthority = new TrustedAuthority();
		int[] keyLengths = { 512, 1024 };
		int[] numberOfExecutionsPerExperimentationForInit = { 20, 2 };
		// initialization of the array of times
		long[][] initializationTimes = new long[keyLengths.length][numberOfExperimentations];
		long[][] keygenTimes = new long[keyLengths.length][numberOfExperimentations];
		long initializationTime;
		for (int i = 0; i < keyLengths.length; i++) {
			for (int j = 0; j < numberOfExperimentations; j++) {
				Parameters[] parametersArray = new Parameters[numberOfExecutionsPerExperimentationForInit[i]];
				initializationTime = System.nanoTime();
				for (int k = 0; k < numberOfExecutionsPerExperimentationForInit[i]; k++) {
					// initialization by the Trusted Authority
					parametersArray[k] = trustedAuthority.initialization(keyLengths[i], certainty, counter);
				}
				initializationTime = System.nanoTime() - initializationTime;
				// collect of the average time per experimentation
				initializationTimes[i][j] = initializationTime / numberOfExecutionsPerExperimentationForInit[i];
				initializationTime = System.nanoTime();
				for (int k = 0; k < numberOfExecutionsPerExperimentationForKeygen; k++) {
					// user key generation by the Trusted Authority
					UserKey publisherKey = trustedAuthority.keygen(publisher.getName(), parametersArray[0]);
					publisher.setKeys(publisherKey.getPrivateKey(), publisherKey.getPublicKey());
					UserKey subscriberKey = trustedAuthority.keygen(subscriber.getName(), parametersArray[0]);
					subscriber.setKeys(subscriberKey.getPrivateKey(), subscriberKey.getPublicKey());
				}
				initializationTime = System.nanoTime() - initializationTime;
				// collect of the average time per experimentation
				keygenTimes[i][j] = initializationTime / numberOfExecutionsPerExperimentationForKeygen;
			}
			if (measure) {
				// printing results : average time and standard deviation for initialization
				System.out.println("average initialization time for " + keyLengths[i] + " bits : "
						+ calculateAverage(initializationTimes[i]));
				System.out.println("standard deviation for initialization time for " + keyLengths[i] + " bits : "
						+ calculateStandardDeviation(initializationTimes[i]));
				System.out.println("average key generation time for " + keyLengths[i] + " bits : "
						+ calculateAverage(keygenTimes[i]));
				System.out.println("standard deviation for key generation time for " + keyLengths[i] + " bits : "
						+ calculateStandardDeviation(keygenTimes[i]));
				System.out.println("");
			}
		}
	}

	/**
	 * calculate the average time of an array of times.
	 * 
	 * @param times the array of times.
	 * @return the average time of the array of times.
	 */
	public static float calculateAverage(final long[] times) {
		float average = 0;
		for (long time : times) {
			average += time;
		}
		average = average / times.length;
		return average;
	}

	/**
	 * calculate the standard deviation for an array of times.
	 * 
	 * @param times the array of times.
	 * @return the standard deviation for the array of times.
	 */
	public static float calculateStandardDeviation(final long[] times) {
		float standardDeviation = 0;
		for (long time : times) {
			standardDeviation += Math.pow(time - calculateAverage(times), 2);
		}
		standardDeviation = standardDeviation / times.length;
		standardDeviation = (float) Math.sqrt(standardDeviation);
		return standardDeviation;
	}
}
