/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): 
 */
package eu.telecomsudparis.ppdebs;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * This class defines hash functions. A collision-resistant hash function is
 * chosen by the Trusted Authority during the initialization phase as a
 * component for the PublicKeySE. Only the SHA family of cryptographic hash
 * functions are used here.
 */
public class HashFunction {
	/**
	 * the message digest of the hash function.
	 */
	private MessageDigest md;

	/**
	 * constructs a hash function.
	 *
	 * @param protoName is the name of the protocol which will be used.
	 * @throws NoSuchAlgorithmException if the protocol does not exist.
	 */
	public HashFunction(final String protoName) throws NoSuchAlgorithmException {
		this.md = MessageDigest.getInstance(protoName);
	}

	/**
	 * gets the implementation of SHA associated with the HashFunction.
	 *
	 * @return md the message digest of the hash function.
	 */
	public MessageDigest getMessageDigest() {
		return this.md;
	}

	/**
	 * sets the message digest of the hash function.
	 *
	 * @param protoName the name of the protocol.
	 * @throws NoSuchAlgorithmException.
	 */
	public void setMessageDigest(final String protoName) throws NoSuchAlgorithmException {
		this.md = MessageDigest.getInstance(protoName);
	}
}
