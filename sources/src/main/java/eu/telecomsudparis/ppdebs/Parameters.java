/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): 
 */
package eu.telecomsudparis.ppdebs;

import java.util.Random;

/**
 * This class defines the concept of parameters, which contains the public and
 * private security parameters for searchable encryption and key policy, as well
 * as prime numbers and a random function regularly used in encryption and
 * decryption operations.
 *
 * Note : the parameters of the public key for searchable encryption are
 * slightly different from the original parameters. We removed the group and the
 * pseudo-random function, which is implemented in both publisher and subscriber
 * classes.
 *
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 *
 */
public class Parameters {
	/**
	 * the public key for searchable encryption of the parameters.
	 */
	private PublicKeySE publicKeySE;
	/**
	 * the public key for key policy of the parameters.
	 */
	private PublicKeyKP publicKeyKP;
	/**
	 * the master key for searchable encryption of the parameters.
	 */
	private MasterKeySE masterKeySE;
	/**
	 * the master key for key policy of the parameters.
	 */
	private MasterKeyKP masterKeyKP;
	/**
	 * the prime numbers defined by the initialization phase run by the Trusted
	 * Authority.
	 */
	private PrimeNumbers primeNumbers;
	/**
	 * the random function defined by the initialization phase run by the Trusted
	 * Authority.
	 */
	private Random randomFunction;

	/**
	 * constructs security parameters.
	 *
	 * @param publicKeySE0    the public key for searchable encryption of the
	 *                        parameters.
	 * @param publicKeyKP0    the public key for key policy of the parameters.
	 * @param masterKeySE0    the master key for searchable encryption of the
	 *                        parameters.
	 * @param masterKeyKP0    the master key for key policy of the parameters.
	 * @param primeNumbers0   the prime numbers defined by the Trusted Authority.
	 * @param randomFunction0 the random function defined by the Trusted Authority.
	 */
	public Parameters(final PublicKeySE publicKeySE0, final PublicKeyKP publicKeyKP0, final MasterKeySE masterKeySE0,
			final MasterKeyKP masterKeyKP0, final PrimeNumbers primeNumbers0, final Random randomFunction0) {
		this.publicKeySE = publicKeySE0;
		this.publicKeyKP = publicKeyKP0;
		this.masterKeySE = masterKeySE0;
		this.masterKeyKP = masterKeyKP0;
		this.primeNumbers = primeNumbers0;
		this.randomFunction = randomFunction0;
	}

	/**
	 * gets the master key for searchable encryption of the parameters.
	 *
	 * @return the master key for searchable encryption of the parameters.
	 */
	public MasterKeySE getMasterKeySE() {
		return this.masterKeySE;
	}

	/**
	 * gets the master key for key policy of the parameters.
	 *
	 * @return the master key for key policy of the parameters.
	 */
	public MasterKeyKP getMasterKeyKP() {
		return this.masterKeyKP;
	}

	/**
	 * gets the public key for searchable encryption of the parameters.
	 *
	 * @return the public key for searchable encryption of the parameters.
	 */
	public PublicKeySE getPublicKeySE() {
		return this.publicKeySE;
	}

	/**
	 * gets the public key for key policy of the parameters.
	 *
	 * @return the public key for key policy of the parameters.
	 */
	public PublicKeyKP getPublicKeyKP() {
		return this.publicKeyKP;
	}

	/**
	 * gets the prime numbers of the parameters.
	 *
	 * @return the prime numbers of the parameters.
	 */
	public PrimeNumbers getPrimeNumbers() {
		return this.primeNumbers;
	}

	/**
	 * gets the random function of the parameters.
	 *
	 * @return the random function of the parameters.
	 */
	public Random getRandomFunction() {
		return this.randomFunction;
	}
}
