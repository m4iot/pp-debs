/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): 
 */
package eu.telecomsudparis.ppdebs;

import java.math.BigInteger;

/**
 * This class defines the concept of encrypted attribute filter, which is used
 * by the subscribers to provide encrypted filters for identifier attributes.
 *
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 */
public class EncryptedAttributeFilter {
	/**
	 * the first numerical component of the encrypted attribute filter.
	 */
	private BigInteger cb1;
	/**
	 * the second numerical component of the encrypted attribute filter.
	 */
	private BigInteger cb2;
	/**
	 * the hash of the encrypted attribute filter.
	 */
	private byte[] hash;
	/**
	 * the id of the encrypted attribute filter, useful during the matching
	 * operation.
	 */
	private int id;

	/**
	 * constructs an encrypted attribute filter.
	 *
	 * @param cb10  the first numerical component of the encrypted attribute filter.
	 * @param cb20  the second numerical component of the encrypted attribute
	 *              filter.
	 * @param hash0 the hash of the encrypted attribute filter.
	 * @param id0   the id of the encrypted attribute filter.
	 */
	public EncryptedAttributeFilter(final BigInteger cb10, final BigInteger cb20, final byte[] hash0, final int id0) {
		this.cb1 = cb10;
		this.cb2 = cb20;
		this.hash = hash0;
		this.id = id0;
	}

	/**
	 * gets the first numerical component of the encrypted attribute filter.
	 *
	 * @return cb1 the first numerical component of the encrypted attribute filter.
	 */
	public BigInteger getCB1() {
		return this.cb1;
	}

	/**
	 * gets the second numerical component of the encrypted attribute filter.
	 *
	 * @return cb2 the second numerical component of the encrypted attribute filter.
	 */
	public BigInteger getCB2() {
		return this.cb2;
	}

	/**
	 * gets the hash of the encrypted attribute filter.
	 *
	 * @return the hash of the encrypted attribute filter.
	 */
	public byte[] getHash() {
		return this.hash;
	}

	/**
	 * gets the id of the encrypted attribute filter.
	 *
	 * @return the id of the encrypted attribute filter.
	 */
	public int getId() {
		return this.id;
	}
}
