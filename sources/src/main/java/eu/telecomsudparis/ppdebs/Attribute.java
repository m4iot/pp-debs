/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Denis Conan
Contributor(s): Pierre Chaffardon and Nathanaël Denis
 */
package eu.telecomsudparis.ppdebs;

/**
 * The class defines the concept of attribute in the structured data model of
 * notifications with KP-ABE. The generic type is the type of the data.
 *
 * Attributes can be used as labels on notifications, as well as filters for the
 * matching process.
 *
 * Limitation: we consider only containers of simple types, e.g. Boolean, Byte,
 * Character, Short, Integer, Long, Float, Double, or String.
 *
 * @param <T> the type of the attribute value, e.g. Boolean, Byte, Character,
 *            Short, Integer, Long, Float, Double, or String.
 *
 * @author Denis Conan
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 */
public class Attribute<T> {
	/**
	 * the name of the attribute.
	 */
	private String name;
	/**
	 * the value of the attribute.
	 */
	private T value;
	/**
	 * the id of the attribute according to the PIA.
	 */
	private int id;

	/**
	 * constructs an attribute.
	 *
	 * @param name0  the name of the attribute.
	 * @param value0 the value of the attribute.
	 * @param id0    the id of the attribute.
	 */
	public Attribute(final String name0, final T value0, final int id0) {
		this.name = name0;
		this.value = value0;
		this.id = id0;
	}

	/**
	 * gets the name of the attribute.
	 *
	 * @return the name of the attribute.
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * gets the value of the attribute.
	 *
	 * @return the value of the attribute.
	 */
	public T getValue() {
		return this.value;
	}

	/**
	 * gets the id of the attribute.
	 *
	 * @return the id of the attribute.
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * sets the value of the id of the attribute.
	 *
	 * @param id0 the id of the attribute.
	 */
	public void setId(final int id0) {
		this.id = id0;
	}
}
