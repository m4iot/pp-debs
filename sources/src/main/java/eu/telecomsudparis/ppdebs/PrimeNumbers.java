/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): 
 */
package eu.telecomsudparis.ppdebs;

import java.math.BigInteger;

/**
 * This class defines the concept of prime numbers, and more especially Sophie
 * Germain prime numbers, generated by the Trusted Authority during the
 * initialization phase.
 *
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 *
 */
public class PrimeNumbers {
	/**
	 * the prime number p, equals to (2 * q + 1).
	 */
	private BigInteger p;
	/**
	 * the Sophie Germain prime number q.
	 */
	private BigInteger q;

	/**
	 * constructs prime numbers.
	 *
	 * @param p0 the prime number p.
	 * @param q0 the Sophie Germain prime number q.
	 */
	public PrimeNumbers(final BigInteger p0, final BigInteger q0) {
		this.p = p0;
		this.q = q0;
	}

	/**
	 * gets the prime number p.
	 *
	 * @return the prime number p.
	 */
	public BigInteger getP() {
		return this.p;
	}

	/**
	 * gets the Sophie Germain prime number q.
	 *
	 * @return the Sophie Germain prime number q.
	 */
	public BigInteger getQ() {
		return this.q;
	}
}
