/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): 
 */
package eu.telecomsudparis.ppdebs;

/**
 * This class defines the Privacy Impact Assessment. Defining a PIA is equate to
 * determining the nature of the attributes of the publication.
 *
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 */
public class PIA {
	/**
	 * the identifiers are attributes which can identify a pulisher. They need to be
	 * encrypted for matching.
	 */
	private String[] identifierAttributes;
	/**
	 * the quasi-identifiers are attributes that identify a publisher when grouped
	 * together. They need to be splitted.
	 */
	private String[] quasiIdentifierAttributes;
	/**
	 * the non sensitive are the others attributes. They can be send in clear
	 * together.
	 */
	private String[] nonSensitiveAttributes;

	/**
	 * constructs a PIA.
	 *
	 * @param identifierAttributes0      the identifier attributes of the PIA
	 * @param quasiIdentifierAttributes0 the quasi-identifier attributes of the PIA.
	 * @param nonSensitiveAttributes0    the non sensitive attributes of the PIA.
	 */
	public PIA(final String[] identifierAttributes0, final String[] quasiIdentifierAttributes0,
			final String[] nonSensitiveAttributes0) {
		this.identifierAttributes = identifierAttributes0;
		this.quasiIdentifierAttributes = quasiIdentifierAttributes0;
		this.nonSensitiveAttributes = nonSensitiveAttributes0;
	}

	/**
	 * gets the identifier attributes of the PIA.
	 *
	 * @return the identifier attributes of the PIA.
	 */
	public String[] getIdentifierAttributes() {
		return this.identifierAttributes;
	}

	/**
	 * gets the quasi-identifier attributes of the PIA.
	 *
	 * @return the quasi-identifier attributes of the PIA.
	 */
	public String[] getQuasiIdentifierAttributes() {
		return this.quasiIdentifierAttributes;
	}

	/**
	 * gets the non sensitive attributes of the PIA.
	 *
	 * @return the non sensitive attributes of the PIA.
	 */
	public String[] getNonSensitiveAttributes() {
		return this.nonSensitiveAttributes;
	}

	/**
	 * checks if an attribute is in the list of identifiers.
	 *
	 * @param attribute the attribute to be tested
	 * @return the index of the attribute if it is in the list, -1 otherwise.
	 */
	public int checkIdentifier(final Attribute<?> attribute) {
		for (int i = 0; i < this.identifierAttributes.length; i++) {
			if (attribute.getName().equals(this.identifierAttributes[i])) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * checks if an attribute is in the list of quasi-identifiers.
	 *
	 * @param attribute the attribute to be tested
	 * @return the index of the attribute if it is in the list, -1 otherwise.
	 */
	public int checkQuasiIdentifier(final Attribute<?> attribute) {
		for (int i = 0; i < this.quasiIdentifierAttributes.length; i++) {
			if (attribute.getName().equals(this.quasiIdentifierAttributes[i])) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * checks if an attribute is in the list of non sensitives.
	 *
	 * @param attribute the attribute to be tested.
	 * @return the index of the attribute if it is in the list, -1 otherwise.
	 */
	public int checkNonSensitive(final Attribute<?> attribute) {
		for (int i = 0; i < this.nonSensitiveAttributes.length; i++) {
			if (attribute.getName().equals(nonSensitiveAttributes[i])) {
				return i;
			}
		}
		return -1;
	}
}
