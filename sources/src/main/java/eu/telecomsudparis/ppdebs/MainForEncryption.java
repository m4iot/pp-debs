/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): Denis Conan
 */
package eu.telecomsudparis.ppdebs;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * This is the Main class for the encryption process.
 * 
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 * @author Denis Conan
 */
public final class MainForEncryption {
	/**
	 * empty constructor.
	 */
	private MainForEncryption() {

	}

	/**
	 * the main method, used to run performance tests on the implementation,
	 * including a warm-up phase to collect garbage before the experiment.
	 * 
	 * @param args
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 * @throws InterruptedException
	 */
	public static void main(final String[] args)
			throws NoSuchAlgorithmException, UnsupportedEncodingException, InterruptedException {
		final int numberOfExperimentations = Integer.parseInt(System.getProperty("numberOfExperimentations"));
		final int numberOfExecutionsPerExperimentation = Integer
				.parseInt(System.getProperty("numberOfExecutionsPerExperimentation"));
		final int keyLength = Integer.parseInt(System.getProperty("keyLength"));
		final int numberOfAttributes = Integer.parseInt(System.getProperty("numberOfAttributes"));
		final int certainty = Integer.parseInt(System.getProperty("certainty"));
		final int counter = Integer.parseInt(System.getProperty("counter"));
		// warm up
		executeExperiment(10, numberOfExecutionsPerExperimentation, keyLength, numberOfAttributes, certainty, counter,
				false);
		// garbage collect before the experiment
		System.gc();
		final long sleepTimeAfterGC = 5000;
		Thread.sleep(sleepTimeAfterGC);
		System.out.println("garbage collected, starting experiment");
		System.out.println("number of experimentations = " + numberOfExperimentations);
		System.out.println("number of executions per experimentation = " + numberOfExecutionsPerExperimentation);
		System.out.println("");
		// experiment
		executeExperiment(numberOfExperimentations, numberOfExecutionsPerExperimentation, keyLength, numberOfAttributes,
				certainty, counter, true);
	}

	/**
	 * executes the experiment to measure performance for the encryption phase on
	 * both publisher and subscriber sides with access brokers.
	 * 
	 * @param numberOfExperimentations             the number of experimentations.
	 * @param numberOfExecutionsPerExperimentation the number of executions per
	 *                                             experimentation.
	 * @param keyLength                            the key length used for
	 *                                             encryption.
	 * @param numberOfAttributes                   the number of attributes for
	 *                                             encryption.
	 * @param certainty                            the certainty of the
	 *                                             initialization phase.
	 * @param counter                              the counter for the
	 *                                             initialization phase.
	 * @param measure
	 * @throws NoSuchAlgorithmException
	 * @throws UnsupportedEncodingException
	 */
	public static void executeExperiment(final int numberOfExperimentations,
			final int numberOfExecutionsPerExperimentation, final int keyLength, final int numberOfAttributes,
			final int certainty, final int counter, final boolean measure)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		// initialization of the constants
		final String publisherName = "Nathanaël";
		final String subscriberName = "Pierre";
		final String prefixOfAttributeNames = "attr";
		TrustedAuthority trustedAuthority = new TrustedAuthority();
		Broker broker = new Broker(0);
		Publisher publisher = new Publisher(publisherName);
		Subscriber subscriber = new Subscriber(subscriberName);
		Attribute<?>[] attributes = new Attribute[numberOfAttributes];
		for (int i = 0; i < numberOfAttributes; i++) {
			attributes[i] = new Attribute<>(prefixOfAttributeNames + i, 1000, -1);
		}
		// initialization of the arrays of times
		final long[][] encryptionOnSubscriberSideTimes = new long[numberOfAttributes][numberOfExperimentations];
		final long[][] encryptionOnPublisherSideTimes = new long[numberOfAttributes][numberOfExperimentations];
		long encryptionTime;
		// initialization by the Trusted Authority
		Parameters parameters = trustedAuthority.initialization(keyLength, certainty, counter);
		// user key generation by the Trusted Authority
		UserKey publisherKey = trustedAuthority.keygen(publisher.getName(), parameters);
		publisher.setKeys(publisherKey.getPrivateKey(), publisherKey.getPublicKey());
		UserKey subscriberKey = trustedAuthority.keygen(subscriber.getName(), parameters);
		subscriber.setKeys(subscriberKey.getPrivateKey(), subscriberKey.getPublicKey());
		for (int i = 1; i <= numberOfAttributes; i++) {
			// initialization of the attributes and the attribute filters
			// of the publisher and the subscriber
			Attribute<?>[] publisherAttributes = Arrays.copyOf(attributes, i);
			Attribute<?>[] subscriberAttributes = Arrays.copyOf(attributes, i);
			for (int j = 0; j < numberOfExperimentations; j++) {
				ReEncryptedAttributeFilter[][] reEncryptedAttributeFiltersArray = new ReEncryptedAttributeFilter[numberOfExecutionsPerExperimentation][numberOfAttributes];
				ReEncryptedEvent[] reEncryptedEventsArray = new ReEncryptedEvent[numberOfExecutionsPerExperimentation];
				encryptionTime = System.nanoTime();
				for (int k = 0; k < numberOfExecutionsPerExperimentation; k++) {
					// encryption of filter by the subscriber
					EncryptedAttributeFilter[] encryptedAttributeFilters = subscriber
							.encryptIdentifierAttributeFilters(subscriberAttributes, parameters);
					// re-encryption of filter by the subscriber access broker
					reEncryptedAttributeFiltersArray[k] = broker
							.reEncryptIdentifierAttributeFilters(encryptedAttributeFilters, subscriber, parameters);
				}
				encryptionTime = System.nanoTime() - encryptionTime;
				// collect of the average time per experimentation
				encryptionOnSubscriberSideTimes[i - 1][j] = encryptionTime / numberOfExecutionsPerExperimentation;
				encryptionTime = System.nanoTime();
				for (int k = 0; k < numberOfExecutionsPerExperimentation; k++) {
					// encryption of event by the publisher
					EncryptedEvent encryptedEvent = publisher.encryptIdentifierAttributes(publisherAttributes,
							parameters);
					// re-encryption of event by the publisher access broker
					reEncryptedEventsArray[k] = broker.reEncryptIdentifierAttributes(encryptedEvent, publisher,
							parameters);
				}
				encryptionTime = System.nanoTime() - encryptionTime;
				// collect of the average time per experimentation
				encryptionOnPublisherSideTimes[i - 1][j] = encryptionTime / numberOfExecutionsPerExperimentation;
			}
			if (measure) {
				// printing results : average time and standard deviation for encryption
				// on subscriber and publisher sides, followed by re-encryption
				System.out.println("average encryption on subscriber side time for " + keyLength + " bits " + "with "
						+ i + " attributes : " + calculateAverage(encryptionOnSubscriberSideTimes[i - 1]));
				System.out.println("standard deviation for encryption on subscriber side time for " + keyLength
						+ " bits : " + "with " + i + " attributes : "
						+ calculateStandardDeviation(encryptionOnSubscriberSideTimes[i - 1]));
				System.out.println("average encryption on publisher side time for " + keyLength + " bits : " + "with "
						+ i + " attributes : " + calculateAverage(encryptionOnPublisherSideTimes[i - 1]));
				System.out.println("standard deviation for encryption on publisher side time for " + keyLength
						+ " bits : " + "with " + i + " attributes : "
						+ calculateStandardDeviation(encryptionOnPublisherSideTimes[i - 1]));
				System.out.println("");
			}
		}
	}

	/**
	 * calculate the average time of an array of times.
	 * 
	 * @param times the array of times.
	 * @return the average time of the array of times.
	 */
	public static float calculateAverage(final long[] times) {
		float average = 0;
		for (long time : times) {
			average += time;
		}
		average = average / times.length;
		return average;
	}

	/**
	 * calculate the standard deviation for an array of times.
	 * 
	 * @param times the array of times.
	 * @return the standard deviation for the array of times.
	 */
	public static float calculateStandardDeviation(final long[] times) {
		float standardDeviation = 0;
		for (long time : times) {
			standardDeviation += Math.pow(time - calculateAverage(times), 2);
		}
		standardDeviation = standardDeviation / times.length;
		standardDeviation = (float) Math.sqrt(standardDeviation);
		return standardDeviation;
	}
}
