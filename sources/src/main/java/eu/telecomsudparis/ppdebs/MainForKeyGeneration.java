/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): Denis Conan
 */
package eu.telecomsudparis.ppdebs;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * This is the Main class for the key generation phase.
 * 
 * @author Pierre Chafffardon
 * @author Nathanaël Denis
 * @author Denis Conan
 *
 */
public final class MainForKeyGeneration {
	/**
	 * private empty constructor.
	 */
	private MainForKeyGeneration() {

	}

	/**
	 * the main method, used to run performance tests on the implementation,
	 * including a warm-up phase to collect garbage before the experiment.
	 * 
	 * @param args command line arguments.
	 * @throws NoSuchAlgorithmException
	 * @throws InterruptedException
	 */
	public static void main(final String[] args) throws NoSuchAlgorithmException, InterruptedException {
		final int numberOfExperimentations = Integer.parseInt(System.getProperty("numberOfExperimentations"));
		final int numberOfExecutionsPerExperimentation = Integer
				.parseInt(System.getProperty("numberOfExecutionsPerExperimentation"));
		final int keyLength = Integer.parseInt(System.getProperty("keyLength"));
		final int numberOfAttributes = Integer.parseInt(System.getProperty("numberOfAttributes"));
		final int certainty = Integer.parseInt(System.getProperty("certainty"));
		final int counter = Integer.parseInt(System.getProperty("counter"));
		// warm up
		executeExperiment(10, numberOfExecutionsPerExperimentation, keyLength, numberOfAttributes, certainty, counter,
				false);
		// garbage collect before the experiment
		final long sleepTimeAfterGC = 5000;
		Thread.sleep(sleepTimeAfterGC);
		System.out.println("garbage collected, starting the experiment");
		System.out.println("number of experimentations = " + numberOfExperimentations);
		System.out.println("number of executions per experimentation = " + numberOfExecutionsPerExperimentation);
		System.out.println("");
		// experiment
		executeExperiment(numberOfExperimentations, numberOfExecutionsPerExperimentation, keyLength, numberOfAttributes,
				certainty, counter, true);
	}

	/**
	 * executes the experiment to measure performance for the user key generation
	 * and the decryption key generation phases run by the Trusted Authority.
	 * 
	 * @param numberOfExperimentations             the number of experimentations.
	 * @param numberOfExecutionsPerExperimentation the number of executions per
	 *                                             experimentation.
	 * @param keyLength                            the key length used for the
	 *                                             decryption filter generation.
	 * @param numberOfAttributes                   the number of attributes of the
	 *                                             subscriber filter.
	 * @param certainty                            the certainty of the
	 *                                             initialization phase.
	 * @param counter                              the counter for the
	 *                                             initialization phase.
	 * @param measure
	 * @throws NoSuchAlgorithmException
	 */
	public static void executeExperiment(final int numberOfExperimentations,
			final int numberOfExecutionsPerExperimentation, final int keyLength, final int numberOfAttributes,
			final int certainty, final int counter, final boolean measure) throws NoSuchAlgorithmException {
		// initialization of the constants
		final String publisherName = "Nathanaël";
		final String subscriberName = "Pierre";
		final String prefixOfAttributeNames = "attr";
		TrustedAuthority trustedAuthority = new TrustedAuthority();
		Publisher publisher = new Publisher(publisherName);
		Subscriber subscriber = new Subscriber(subscriberName);
		Attribute<?>[] attributes = new Attribute[numberOfAttributes];
		for (int i = 0; i < numberOfAttributes; i++) {
			attributes[i] = new Attribute<>(prefixOfAttributeNames + i, 1000, -1);
		}
		// initialization of the array of times
		final long[][] decryptionFilterGenerationTimes = new long[numberOfAttributes][numberOfExperimentations];
		long decryptionFilterGenerationTime;
		// initialization by the Trusted Authority
		Parameters parameters = trustedAuthority.initialization(keyLength, certainty, counter);
		// user key generation by the Trusted Authority
		UserKey publisherKey = trustedAuthority.keygen(publisher.getName(), parameters);
		publisher.setKeys(publisherKey.getPrivateKey(), publisherKey.getPublicKey());
		UserKey subscriberKey = trustedAuthority.keygen(subscriber.getName(), parameters);
		subscriber.setKeys(subscriberKey.getPrivateKey(), subscriberKey.getPublicKey());
		for (int i = 1; i <= numberOfAttributes; i++) {
			// initialization of the attribute filters of the subscriber
			Attribute<?>[] subscriberAttributes = Arrays.copyOf(attributes, i);
			for (int j = 0; j < numberOfExperimentations; j++) {
				DecryptionKey[][] decryptionKeysArray = new DecryptionKey[numberOfExecutionsPerExperimentation][i];
				decryptionFilterGenerationTime = System.nanoTime();
				for (int k = 0; k < numberOfExecutionsPerExperimentation; k++) {
					// decryption filter generation by the Trusted Authority
					decryptionKeysArray[k] = trustedAuthority.createDecryptionKeys(subscriberAttributes, parameters);
				}
				decryptionFilterGenerationTime = System.nanoTime() - decryptionFilterGenerationTime;
				// collect of the average time for an experimentation
				decryptionFilterGenerationTimes[i - 1][j] = decryptionFilterGenerationTime
						/ numberOfExecutionsPerExperimentation;
			}
			if (measure) {
				// printing results : average time and standard deviation for decryption filter
				// generation
				System.out.println("average decryption filter generation time for " + keyLength + " bits : " + "with "
						+ i + " attributes : " + calculateAverage(decryptionFilterGenerationTimes[i - 1]));
				System.out.println("standard deviation for decryption filter generation time for " + keyLength
						+ " bits : " + "with " + i + " attributes : "
						+ calculateStandardDeviation(decryptionFilterGenerationTimes[i - 1]));
				System.out.println("");
			}
		}
	}

	/**
	 * calculate the average time of an array of times.
	 * 
	 * @param times the array of times.
	 * @return the average time of the array of times.
	 */
	public static float calculateAverage(final long[] times) {
		float average = 0;
		for (long time : times) {
			average += time;
		}
		average = average / times.length;
		return average;
	}

	/**
	 * calculate the standard deviation for an array of times.
	 * 
	 * @param times the array of times.
	 * @return the standard deviation for the array of times.
	 */
	public static float calculateStandardDeviation(final long[] times) {
		float standardDeviation = 0;
		for (long time : times) {
			standardDeviation += Math.pow(time - calculateAverage(times), 2);
		}
		standardDeviation = standardDeviation / times.length;
		standardDeviation = (float) Math.sqrt(standardDeviation);
		return standardDeviation;
	}
}
