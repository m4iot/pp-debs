/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): 
 */
package eu.telecomsudparis.ppdebs;

import java.math.BigInteger;

/**
 * This class defines the concept of re-encrypted event, which is created by the
 * broker on an already encrypted event.
 *
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 *
 */
public class ReEncryptedEvent {
	/**
	 * the set of trapdoors of the re-encrypted event. These trapdoors are different
	 * than the ones in the already encrypted event.
	 */
	private TrapDoorForBroker[] trapDoors;
	/**
	 * the encrypted message of the re-encrypted event, which is the same than the
	 * one in the already encrypted event.
	 */
	private BigInteger encryptedMessage;
	/**
	 * the key of the re-encrypted event, which is the same also.
	 */
	private BigInteger key;
	/**
	 * the set of shared secret of the re-encrypted event, which are the same also.
	 */
	private SharedSecret[] tFunctionOnAttributes;

	/**
	 * constructs a re-encrypted event.
	 *
	 * @param trapDoors0             the set of trapdoors of the re-encrypted event.
	 * @param encryptedMessage0      the encrypted message of the re-encrypted
	 *                               event.
	 * @param key0                   the key of the re-encrypted event.
	 * @param tFunctionOnAttributes0 the set of shared secrets of the re-encrypted
	 *                               event.
	 */
	public ReEncryptedEvent(final TrapDoorForBroker[] trapDoors0, final BigInteger encryptedMessage0,
			final BigInteger key0, final SharedSecret[] tFunctionOnAttributes0) {
		this.trapDoors = trapDoors0;
		this.encryptedMessage = encryptedMessage0;
		this.key = key0;
		this.tFunctionOnAttributes = tFunctionOnAttributes0;
	}

	/**
	 * gets the set of trapdoors of the re-encrypted event.
	 *
	 * @return the trapdoors of the re-encrypted event.
	 */
	public TrapDoorForBroker[] getTrapDoors() {
		return this.trapDoors;
	}

	/**
	 * gets the encrypted message of the re-encrypted event.
	 *
	 * @return the encrypted message of the re-encrypted event.
	 */
	public BigInteger getEncryptedMessage() {
		return this.encryptedMessage;
	}

	/**
	 * gets the key of the re-encrypted event.
	 *
	 * @return the key of the re-encrypted event.
	 */
	public BigInteger getKey() {
		return this.key;
	}

	/**
	 * gets the set of shared secrets of the re-encrypted event.
	 *
	 * @return the shared secrets of the re-encrypted event.
	 */
	public SharedSecret[] getTFunctionOnAttributes() {
		return this.tFunctionOnAttributes;
	}
}
