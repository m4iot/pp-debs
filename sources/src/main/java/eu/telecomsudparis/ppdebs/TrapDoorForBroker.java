/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): 
 */
package eu.telecomsudparis.ppdebs;

import java.math.BigInteger;

/**
 * This class defines the concept of trapdoor on broker side, e.g. in
 * re-encrypted events. These are different from the trapdoors on publisher
 * side.
 *
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 *
 */
public class TrapDoorForBroker {
	/**
	 * the numerical value of the trapdoor on broker side.
	 */
	private BigInteger td;
	/**
	 * the id of the trapdoor on broker side.
	 */
	private int id;

	/**
	 * constructs a trapdoor on broker side.
	 *
	 * @param td0 the numerical value of the trapdoor on broker side.
	 * @param id0 the id of the trapdoor on broker side.
	 */
	public TrapDoorForBroker(final BigInteger td0, final int id0) {
		this.td = td0;
		this.id = id0;
	}

	/**
	 * gets the numerical value of the trapdoor on broker side.
	 *
	 * @return td the numerical value of the trapdoor on broker side.
	 */
	public BigInteger getTD() {
		return this.td;
	}

	/**
	 * gets the id of the trapdoor on broker side.
	 *
	 * @return the id of the trapdoor on broker side.
	 */
	public int getId() {
		return this.id;
	}
}
