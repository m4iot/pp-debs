/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): 
 */
package eu.telecomsudparis.ppdebs;

import java.math.BigInteger;

/**
 * This class defines the concept of re-encrypted attribute filter, which is
 * created by the broker on an already encrypted attribute filter for an
 * identifier in this case.
 *
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 *
 */
public class ReEncryptedAttributeFilter {
	/**
	 * the numerical component of the re-encrypted attribute filter.
	 */
	private BigInteger cb;
	/**
	 * the hash of the re-encrypted attribute filter.
	 */
	private byte[] hash;
	/**
	 * the id of the re-encrypted attribute filter, which is similar to the id of
	 * the corresponding attribute.
	 */
	private int id;

	/**
	 * constructs a re-encrypted attribute filter.
	 *
	 * @param cb0   the numerical component of the re-encrypted attribute filer.
	 * @param hash0 the hash of the re-encrypted attribute filter.
	 * @param id0   the id of the re-encrypted attribute filter.
	 */
	public ReEncryptedAttributeFilter(final BigInteger cb0, final byte[] hash0, final int id0) {
		this.cb = cb0;
		this.hash = hash0;
		this.id = id0;
	}

	/**
	 * gets the numerical component of the re-encrypted attribute filter.
	 *
	 * @return cb the numerical component of the re-encrypted attribute filter.
	 */
	public BigInteger getCB() {
		return this.cb;
	}

	/**
	 * gets the hash of the re-encrypted attribute filter.
	 *
	 * @return the hash of the re-encrypted attribute filter.
	 */
	public byte[] getHash() {
		return this.hash;
	}

	/**
	 * gets the id of the re-encrypted attribute filter.
	 *
	 * @return the id of the re-encrypted attribute filter.
	 */
	public int getId() {
		return this.id;
	}
}
