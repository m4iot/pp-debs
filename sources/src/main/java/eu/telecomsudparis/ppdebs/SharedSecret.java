/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): 
 */
package eu.telecomsudparis.ppdebs;

import java.math.BigInteger;

/**
 * This class defines the concept of shared secret, obtained by the T function
 * applied on an attribute. This is used during the decryption process run by
 * the subscriber.
 *
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 *
 */
public class SharedSecret {
	/**
	 * the value of the shared secret.
	 */
	private BigInteger value;
	/**
	 * the id of the shared secret.
	 */
	private int id;

	/**
	 * constructs a shared secret.
	 *
	 * @param value0 the value of the shared secret.
	 * @param id0    the id of the shared secret.
	 */
	public SharedSecret(final BigInteger value0, final int id0) {
		this.value = value0;
		this.id = id0;
	}

	/**
	 * gets the value of the shared secret.
	 *
	 * @return the value of the shared secret.
	 */
	public BigInteger getValue() {
		return this.value;
	}

	/**
	 * gets the id of the shared secret.
	 *
	 * @return the id of the shared secret.
	 */
	public int getId() {
		return this.id;
	}
}
