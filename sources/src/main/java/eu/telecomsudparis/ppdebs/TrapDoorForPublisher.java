/**
This file is part of the PP-DEBS middleware.

Copyright (C) 2019-2020 Télécom SudParis

The PP-DEBS software is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

The PP-DEBS software platform is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the PP-DEBS platform. If not, see <http://www.gnu.org/licenses/>.

Initial developer(s): Pierre Chaffardon and Nathanaël Denis
Contributor(s): 
 */
package eu.telecomsudparis.ppdebs;

import java.math.BigInteger;

/**
 * This class defines the concepts of trapdoor on publisher side, e.g. in
 * encrypted events. These are different than the ones on broker side in
 * re-encrypted events.
 *
 * @author Pierre Chaffardon
 * @author Nathanaël Denis
 *
 */
public class TrapDoorForPublisher {
	/**
	 * the first numerical component of the trapdoor on publisher side.
	 */
	private BigInteger td1;
	/**
	 * the second numerical component of the trapdoor on publisher side.
	 */
	private BigInteger td2;
	/**
	 * the id of the trapdoor on publisher side.
	 */
	private int id;

	/**
	 * constructs a trapdoor on publisher side.
	 *
	 * @param td10 the first numerical component of the trapdoor on publisher side.
	 * @param td20 the second numerical component of the trapdoor on publisher side.
	 * @param id0  the id of the trapdoor on publisher side.
	 */
	public TrapDoorForPublisher(final BigInteger td10, final BigInteger td20, final int id0) {
		this.td1 = td10;
		this.td2 = td20;
		this.id = id0;
	}

	/**
	 * gets the first numerical component of the trapdoor on publisher side.
	 *
	 * @return td1 the first numerical component of the trapdoor on publisher side.
	 */
	public BigInteger getTD1() {
		return this.td1;
	}

	/**
	 * gets the second numerical component of the trapdoor on publisher side.
	 *
	 * @return td2 the second numerical component of the trapdoor on publisher side.
	 */
	public BigInteger getTD2() {
		return this.td2;
	}

	/**
	 * gets the id of the trapdoor on publisher side.
	 *
	 * @return the id of the trapdoor on publisher side.
	 */
	public int getId() {
		return this.id;
	}
}
